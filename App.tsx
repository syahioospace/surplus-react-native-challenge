import React from 'react';

import { NavigationContainer } from "@react-navigation/native";
import Routes from "./src/routes/routes";

import { StatusBar } from 'expo-status-bar'
import { ThemeProvider } from "styled-components";
import { AppProvider } from './src/context';
//import AppLoading from 'expo-app-loading';

import {
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold,
  useFonts,
} from '@expo-google-fonts/inter';

import theme from "./src/layouts/theme";

const App = () => {

  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold,
  });

  if (!fontsLoaded) {
    //return <AppLoading/>
  }
  return (
    <NavigationContainer>
      <StatusBar style="dark" backgroundColor="#FFF" translucent />
      <ThemeProvider theme={theme}>
      <AppProvider>
        <Routes />
      </AppProvider>
      </ThemeProvider>
    </NavigationContainer>
  )
}

export default App