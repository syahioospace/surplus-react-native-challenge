import React, { memo, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import styled from "styled-components/native";
import Header from '../components/Header';
import Button from '../components/Button';
import InputForm from '../components/InputForm';
import Logo from '../components/Logo';
import theme from '../../../layouts/theme';
import { Navigation } from "../../../types/Navigation";
import { emailValidation } from "../../../utils/EmailValidation";
import { nameValidation } from "../../../utils/NameValidation";
import { passwordValidation } from "../../../utils/PasswordValidation";

type Props = {
  navigation: Navigation;
};

const RegisterScreen = ({ navigation }: Props) => {
  const [name, setName] = useState({ value: '', error: '' });
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const _onSignUp = () => {
    const nameError = nameValidation(name.value);
    const emailError = emailValidation(email.value);
    const passwordError = passwordValidation(password.value);

    if (emailError || passwordError || nameError) {
      setName({ ...name, error: nameError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }

    navigation.navigate('Pokedex');
  };

  return (
    <Container>
      <Logo />
      <Header>Register</Header>

      <InputForm
        label="Name"
        returnKeyType="next"
        value={name.value}
        onChangeText={text => setName({ value: text, error: '' })}
        error={!!name.error}
        errorText={name.error}
      />

      <InputForm
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <InputForm
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <Button mode="contained" onPress={_onSignUp} style={styles.button}>
        Sign Up
      </Button>

      <View style={styles.row}>
        <Text style={styles.label}>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  label: {
    color: theme.colors.grey,
  },
  button: {
    marginTop: 24,
    borderColor: theme.colors.blue
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.blue,
  },
});

const Container = styled.View`
  flex: 1;
  width: 100%;
  padding: 50px;
  align-items: center;
  alig-self: center;
  justify-content: center;
`

export default RegisterScreen;
