import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Button as PaperButton } from 'react-native-paper';
import theme from '../../../layouts/theme';

type Props = React.ComponentProps<typeof PaperButton>;

const Button = ({ mode, style, children, ...props }: Props) => (
  <PaperButton
    style={[
      styles.button,
      mode === 'outlined' && { backgroundColor: 'transparent'},
      style,
    ]}
    theme={styles.buttonTheme}
    labelStyle={styles.text}
    mode={mode}
    {...props}
  >
    {children}
  </PaperButton>
);

const styles = StyleSheet.create({
  buttonTheme: {
    colors: {
      placeholder: theme.colors.blue,
      text: 'white', primary: theme.colors.blue,
      underlineColor: 'transparent',
      background: '#0f1a2b',
    }
  },
  button: {
    width: '100%',
    marginVertical: 10,
    borderColor: theme.colors.grey,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
  },
});

export default Button;
