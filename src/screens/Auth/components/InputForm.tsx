import React, { memo } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput as Input } from 'react-native-paper';
import { transparent } from 'react-native-paper/lib/typescript/styles/colors';
import theme from '../../../layouts/theme';

type Props = React.ComponentProps<typeof Input> & { errorText?: string };

const InputForm = ({ errorText, ...props }: Props) => (
  <View style={styles.container}>
    <Input
      style={styles.input}
      selectionColor={theme.colors.black}
      underlineColor="transparent"
      theme={styles.inputTheme}
      mode="outlined"
      {...props}
    />
    {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  input: {
    backgroundColor: 'transparent',
  },
  inputTheme: {
    colors: {
      placeholder: theme.colors.blue,
      text: theme.colors.blue, primary: theme.colors.blue,
      underlineColor: 'transparent',
      background: '#0f1a2b'
    }
  },
  error: {
    fontSize: 14,
    color: theme.colors.red,
    paddingHorizontal: 4,
    paddingTop: 4,
  },
});

export default InputForm;
