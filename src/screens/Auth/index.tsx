import React from 'react'
import styled from "styled-components/native";
import Header from './components/Header'
import Paragraph from './components/Paragraph'
import Button from './components/Button'
import Logo from './components/Logo'
import { Navigation } from '../../types/Navigation'

type StartScreenProps = {
  navigation: Navigation
}

const StartScreen = ({ navigation }: StartScreenProps) => {
  return (
    <Container>
      <Logo />
      <Header>Login / Register</Header>
      <Paragraph>
        Login or register to start using the app.
      </Paragraph>
      <Button mode="contained" onPress={() => navigation.navigate('Login')}>
        Login
      </Button>
      <Button
        mode="outlined"
        onPress={() => navigation.navigate('Register')}
      >
        Sign Up
      </Button>
    </Container>
  )
}

const Container = styled.View`
  flex: 1;
  width: 100%;
  padding: 50px;
  align-items: center;
  alig-self: center;
  justify-content: center;
`

export default StartScreen