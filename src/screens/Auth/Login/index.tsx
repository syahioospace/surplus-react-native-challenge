import React, { memo, useState } from "react";
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import styled from "styled-components/native";
import Header from "../components/Header";
import Button from "../components/Button";
import InputForm from "../components/InputForm";
import Logo from "../components/Logo";
import theme from '../../../layouts/theme';
import { emailValidation } from "../../../utils/EmailValidation";
import { passwordValidation } from "../../../utils/PasswordValidation";
import { Navigation } from "../../../types/Navigation";

//import { Container } from "../styles";

type navigationProps = {
  navigation: Navigation
}

const Login = ({ navigation }: navigationProps) => {
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const _onLogin = () => {
    const emailError = emailValidation(email.value);
    const passwordError = passwordValidation(password.value);

    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return
    }

    navigation.navigate('Pokedex');
  }

  return (
    <Container>
      <Logo />
      <Header>Login</Header>
      <InputForm
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <InputForm
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ForgotPassword')}
        >
          <Text style={styles.label}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>

      <Button mode="contained" onPress={_onLogin}>
        Login
      </Button>
      <View style={styles.row}>
        <Text style={styles.label}>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Container>
  )
}

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.grey,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.blue,
  },
});

const Container = styled.View`
  flex: 1;
  width: 100%;
  padding: 50px;
  align-items: center;
  alig-self: center;
  justify-content: center;
`

export default Login;