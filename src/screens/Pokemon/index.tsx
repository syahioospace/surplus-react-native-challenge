import React, { useMemo } from "react";
import { StatusBar } from 'expo-status-bar';
import { useRoute } from "@react-navigation/core";

import { PokemonList as PokemonType } from '../../types/pokemonList'
import { getColorByPokemonType } from "../../utils";
import Header from './Header'
import Summery from "./Summary";

import { Container, Content, DetailContainer } from "./styles";

type RouteParams = {
  pokemon: PokemonType
  type: string
}

const Pokemon = () => {
  const route = useRoute()
  const { pokemon, type } = route.params as RouteParams

  const backgroundColor = useMemo(
    () => getColorByPokemonType(type),
    [type]
  )

  return (
    <>
      <StatusBar style="light" backgroundColor="transparent" translucent />
      <Container
        style={{
          backgroundColor,
        }}
      >
        <Content>
          <Header pokemon={pokemon}/>
          <Summery pokemon={pokemon} type={type}/>
        </Content>
        <DetailContainer>

        </DetailContainer>
      </Container>
    </>
  )
}

export default Pokemon