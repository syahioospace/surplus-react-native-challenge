import React, { useCallback } from "react";
import { View } from "react-native";
import { Feather as Icon } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

import { PokemonList as Pokemon } from "../../../types/pokemonList";
import Text from "../../../components/Text";
import Pokeball from "../../../components/Pokeball";

import { Container, BackButton } from "./styles";
import theme from "../../../layouts/theme";

type HeaderProps = {
  pokemon: Pokemon
}

const Header = ({ pokemon }: HeaderProps) => {
  const navigation = useNavigation();

  const addZeros = useCallback(
    (id: number | string) => {
      let newId = id.toString()
      return newId.padStart(3, '0')
    },
    [pokemon.id],
  )

  const handleBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  return (
    <Container>
      <Pokeball
        width={100}
        height={100}
        style={{
          position: 'absolute',
          right: -10,
        }}
      />

      <BackButton onPress={handleBack}>
        <Icon name="arrow-left" color={theme.colors.white} size={24}/>
      </BackButton>

      <View>
        <Text variant='body1' color='white' bold>
          {pokemon.name}
        </Text>
      </View>

      <View>
        <Text variant='body3' color='white' bold>
          #{addZeros(pokemon.id)}
        </Text>
      </View>

    </Container>
  )
}

export default Header;