import styled from "styled-components/native";
import { Animated } from "react-native";

export const Container = styled.View`
  flex: 1;
`

export const Content = styled.View``

export const DetailContainer = styled(Animated.View)`
  flex: 1;
  position: relative;
`