import React from "react";
import { Animated } from "react-native";
import { SharedElement } from "react-navigation-shared-element";
import Pokeball from "../../../components/Pokeball";

import Text from "../../../components/Text";
import { PokemonList as Pokemon } from "../../../types/pokemonList";

import {
  Container,
  Header,
  Row,
  ImageContainer,
  PokemonImage,
} from "./styles";

type SummaryProps = {
  pokemon: Pokemon
  type: string
}

const Summery = ({ pokemon, type }: SummaryProps) => {
  return (
    <>
      <Pokeball
        width={350}
        height={350}
        style={{
          position: 'absolute',
          bottom: -50,
          alignSelf: 'center',
        }}
      />

      <Container>
        <Header>
          <Row>
            <SharedElement
              id={`pokemon.1.name`}
              style={{ alignItems: 'flex-start' }}
            >
              <Text variant="title" color="white">
                {pokemon.name.toUpperCase()}
              </Text>
            </SharedElement>

            <Animated.View>
              <Text variant="body1" color="white" bold>
                {type.toUpperCase()}
              </Text>
            </Animated.View>
          </Row>

        </Header>

        <ImageContainer>
          <PokemonImage uri={pokemon.image}/>
        </ImageContainer>
      </Container>
    </>
  )
}

export default Summery;