import React, { useMemo } from "react";
import { View, Animated, Alert } from "react-native";

import Text from "../../components/Text";
import { Pokemons } from "../../hooks/pokemons";

import PokemonCard from './PokemonCard';
import Header from "../../components/Header";

import { Container, PokemonsList } from "./styles";

const Home = () => {
  const { pokemons, fetchPokemons } = Pokemons()
  const opacity = useMemo(() => new Animated.Value(0), []);
  //console.log(pokemons)
  return (
    <Container>
      <Header>
        <Text variant='title' color='grey'>Pokedex</Text>
      </Header>
      <PokemonsList
        data={pokemons}
        showsVerticalScrollIndicator={false}
        onEndReached={fetchPokemons}
        onEndReachedThreshold={0.1}
        removeClippedSubviews
        keyExtractor={pokemon => pokemon.id}
        numColumns={2}
        renderItem={({ item: pokemon, index }) => {
          return (
            <PokemonCard
              pokemon={pokemon}
            />
          )
        }}
      />
    </Container >
  )
}

export default Home