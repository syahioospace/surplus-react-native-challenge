import React, { useMemo, useCallback, useState, useEffect } from 'react'
import { useNavigation } from '@react-navigation/core'
import type { Pokemon } from '../../../types/pokemon'
import { pokemonApi, baseUrl } from '../../../services/api'
import { getColorByPokemonType } from '../../../utils'
import Text from '../../../components/Text'
import { SharedElement } from 'react-navigation-shared-element';
import Pokeball from '../../../components/Pokeball'
import { Container, PokemonImage, Button, PokedexNumber } from './styles'

type PokemonCardProps = {
  pokemon: Pokemon
}

const PokemonCard = ({
  pokemon,
}: PokemonCardProps) => {
  const [type, setType] = useState('normal')
  const navigation = useNavigation()

  const pokemonType = async (id: number) => {
    const resp = await pokemonApi.get(`${baseUrl}/pokemon/${id}`)
    setType(resp.data.types[0].type.name)
  }

  const backgroundColor = useMemo(
    () => getColorByPokemonType(type),
    [type],
  );

  const addZeros = useCallback(
    (id: number) => {
      let newId = id.toString()
      return newId.padStart(3, '0')
    },
    [pokemon.id],
  )

  const handleNavigation = useCallback(() => {
    navigation.navigate('Pokemon', {
      pokemon,
      type
    })
  }, [pokemon, type])

  useEffect(() => {
    pokemonType(pokemon.id)
  }, [pokemon.id])

  return (
    <Container>
      <Button
        style={{
          backgroundColor,
        }}
        onPress={handleNavigation}
      >
        <SharedElement
          id={`pokemon.${pokemon.id}.name`}
          style={{ alignItems: 'flex-start' }}
        >
          <Text color="white" bold>
            {pokemon.name.toUpperCase()}
          </Text>
        </SharedElement>
        <PokedexNumber>
          #{addZeros(pokemon.id)}
        </PokedexNumber>
        <SharedElement
          id={`pokemon.${pokemon.id}.image`}
          style={{ position: 'absolute', bottom: 4, right: 4 }}
        >
          <PokemonImage uri={pokemon.image} />
        </SharedElement>

        <Pokeball
          width={80}
          height={80}
          style={{
            position: 'absolute',
            right: -8,
            bottom: -8,
          }}
        />
        <Text variant='type' color="black"
          style={{
            position: 'absolute',
            left: 16,
            bottom: 10,
          }}
        >
          {type.toUpperCase()}
        </Text>
      </Button>
    </Container>
  )
}

export default PokemonCard