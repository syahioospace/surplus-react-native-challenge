import React from 'react';
import { Animated, RegisteredStyle, ViewStyle } from 'react-native';

import pokeballImage from '../../assets/pokeball.png';
import theme from '../../layouts/theme';

import { Container, PokeballImage } from './styles';

export type PokeballProps = {
  width: number;
  height: number;
  color?: string;
  style?: RegisteredStyle<ViewStyle> | Animated.WithAnimatedObject<ViewStyle>;
};

const Pokeball = ({
  width,
  height,
  color,
  style,
}: PokeballProps) => {

  return (
    <Container style={style}>
      <PokeballImage
        source={pokeballImage}
        width={width}
        height={height}
        style={{
          tintColor: color,
        }}
      />
    </Container>
  );
};

Pokeball.defaultProps = {
  color: `${theme.colors.white}20`,
  widthRotate: false,
  style: {},
};

export default Pokeball;
