import { useContext, useEffect, useRef, useState } from 'react'
import { baseUrl, pokemonApi, pokemonImage } from '../services/api'
import { Context } from '../context'
import { Result, PokemonList, PokemonsResponse } from '../types/PokemonList'

export const Pokemons = () => {
  const [pokemons, setPokemons] = useState<PokemonList[]>([])
  const { dispatch } = useContext(Context)

  const pokemonUrl = useRef(`${baseUrl}/pokemon?limit=20`)

  const pokemonsList = (list: Result[]) => {
    const appendPokemons = list.map(({ name, url }) => {
      const splitUrl = url.split('/')
      const id = splitUrl[splitUrl.length - 2]
      const image = `${pokemonImage}/${id}.png`
      return { id, image, name }
    })

    setPokemons([...pokemons, ...appendPokemons])
    dispatch({ type: 'SET_POKEMONS', payload: [...pokemons, ...appendPokemons] })
  }

  const pokemonType = async (id: number) => {
    const resp = await pokemonApi.get(`${baseUrl}/pokemon/${id}`)
    return resp.data.types[0].type.name
  }

  const fetchPokemons = async () => {
    try {
      const response = await pokemonApi.get<PokemonsResponse>(pokemonUrl.current)
      pokemonUrl.current = response.data.next
      pokemonsList(response.data.results)
    } catch (error) {
      setPokemons([])
    }
  }

  useEffect(() => {
    fetchPokemons()
  }, [])

  return {
    pokemons,
    fetchPokemons,
    pokemonType
  }
}