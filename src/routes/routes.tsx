import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Pokedex";
import StartScreen from "../screens/Auth";
import Login from "../screens/Auth/Login";
import Register from "../screens/Auth/Register";
import Pokemon from "../screens/Pokemon";

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="StartScreen" component={StartScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Pokedex" component={Home} />
      <Stack.Screen name="Pokemon" component={Pokemon} />
    </Stack.Navigator>
  )
}

export default Routes