export const POKEMON_TYPE_COLORS = {
  normal: '#919191',
  fire: '#E3350D',
  grass: '#4DAD5B',
  water: '#30A7D7',
  bug: '#37796C',
  poison: '#855AC9',
  fighting: '#C03028',
  flying: '#A890F0',
  ground: '#E0C068',
  rock: '#B8A038',
  ghost: '#705898',
  steel: '#B8B8D0',
  electric: '#FFCE4B',
  psychic: '#F85888',
  ice: '#98D8D8',
  dragon: '#7038F8',
  dark: '#705848',
  fairy: '#EE99AC',
} as const;

export const HEADER_HEIGHT = 64;

export const POKEMON_SUMMARY_HEIGHT = 360;

export const API_OFFSET = 18;
