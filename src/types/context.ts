import React from 'react';
import type { PokemonList } from './pokemonList';

export type ContextProps = {
  state: StateProps
  dispatch: React.Dispatch<ActionProps>
}

export type ProviderProps = {
  children: React.ReactNode
}

export type StateProps = {
  pokemons: PokemonList[],
}

export type ActionProps = { type: 'SET_POKEMONS'; payload: PokemonList }