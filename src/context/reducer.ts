import type { ActionProps, StateProps } from "../types/context"

export const initState: StateProps = {
  pokemons: [],
  pokemon: {
    about: null
  }
}

export const stateReducer = (state: StateProps, action: ActionProps): StateProps => {
  switch (action.type) {
    case 'SET_POKEMONS':
      return {
        ...state,
        pokemons: action.payload
      }

    default:
      return state
  }
}