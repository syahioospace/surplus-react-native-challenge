import React, { createContext, useReducer } from 'react'
import { initState, stateReducer } from './reducer'
import { ProviderProps, ContextProps } from '../types/context'

export const Context = createContext({} as ContextProps)

export const AppProvider = ({ children }: ProviderProps) => {
  const [state, dispatch] = useReducer(stateReducer, initState)

  return (
    <Context.Provider
      value={{
        state,
        dispatch
      }}
    >
      {children}
    </Context.Provider>
  )
}