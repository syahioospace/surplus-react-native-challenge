import axios from "axios";

export const pokemonApi = axios.create();

export const pokemonImage = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork'
export const baseUrl = 'https://pokeapi.co/api/v2'